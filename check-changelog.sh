#!/usr/bin/env sh

while getopts "t:" opt; do
  case $opt in
    t)
      echo "Target branch is $OPTARG"
      TARGET="$OPTARG"
      ;;
    ?)
      echo "Invalid option: -$OPTARG"
      exit 1
      ;;
  esac
done
shift $((OPTIND-1))

if [ -z "$TARGET" ]; then
  echo "-t is required"
  exit 1
fi

git fetch origin "$TARGET"

changed_files="$(git diff --name-only origin/"$TARGET")"
if echo "$changed_files" | grep -q "CHANGELOG" 1>/dev/null; then
    echo "Nice! changelog was updated"
else
    echo "ERROR: You must make a change to the CHANGELOG file before pushing."
    exit 1
fi
