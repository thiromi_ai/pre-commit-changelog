# pre-commit-changelog

Warns every time you try to push a branch without updating CHANGELOG

## Getting started

This is a hook for the pre-commit framework. Please make sure it's installed before proceeding

### pipx

```
pipx install pre-commit
```

### homebrew

```
brew install pre-commit
```

## Installation

Add this to your `.pre-commit-config.yaml` file:

```yaml
  - repo: https://gitlab.com/thiromi_ai/pre-commit-changelog
    rev: 0.1.0
    hooks:
      - id: detect-changelog-changes
        args: [-t, "main"]
        stages:
          - push
```

whereas `-t main` means the target branch is `main`.

This is a `pre-push` hook, therefore you'll need to specify the hook while installing into your project:

```
pre-commit install --hook-type pre-push
```

You can install multiple hooks at the same time (you can use the following to put in your project's README)

```
pre-commit install --hook-type pre-commit --hook-type pre-push
```

## Usage

You can either run it whenever you have changes in your local, and you're going to push to the remote or by using

```
pre-commit run -a --hook-stage pre-push
```

## Caveats

As the check script uses git, it doesn't integrate well with CI. Take that into account while using it
